export class List {
    constructor(books) {
        this.list = document.createElement('ul');
        this.books = books;
    }

    createElements() {
        this.list.classList.add('list');
        for (let i = 0; i < this.books.length; i++) {
            try {
                for (let key in this.books[i]) {
                    if (this.books[i].author && this.books[i].name && this.books[i].price) {
                        const listItem = document.createElement('li');
                        listItem.classList.add('list__item');
                        this.list.appendChild(listItem);
                        const listItemP = document.createElement('p');
                        listItemP.classList.add(key);
                        listItem.appendChild(listItemP);
                        listItemP.innerHTML = `<strong>${key.toLocaleUpperCase()}:</strong> ${this.books[i][key]}`;
                    } else if(!this.books[i].author){
                        throw new Error(`Index Object: ${i}. Missing properties: author`);
                    } else if(!this.books[i].name){
                        throw new Error(`Index Object: ${i}. Missing properties: name`);
                    } else if(!this.books[i].price){
                        throw new Error(`Index Object: ${i}. Missing properties: price`);
                    }
                }
            } catch (error) {
                console.error(error.message)
            }
        }
    }

    render(place = `body`) {
        this.createElements();
        document.querySelector(place).append(this.list);
    }
}